[
  {
    "id": 1,
    "type": "app",
    "expires": 1459439999000,
    "reactiveExpires": 1462031999000,
    "inMarket": false,
    "package": "com.ss.android.essay.joke",
    "app": "http://192.168.1.11:9000/www/assets/apk/com.ss.android.essay.joke_4.4.0_440.apk",
    "checksum": "333e298cd6e7df619fb8b4cc274e5268",
    "cache": "http://192.168.1.11:9000/www/track/cache.js?i=easyjoke",
    "show": "http://192.168.1.11:9000/www/track/show.js?i=easyjoke",
    "click": "http://192.168.1.11:9000/www/track/click.js?i=easyjoke",
    "download": "http://192.168.1.11:9000/www/track/download.js?i=easyjoke",
    "install": "http://192.168.1.11:9000/www/track/install.js?i=easyjoke",
    "activate": "http://192.168.1.11:9000/www/track/activate.js?i=easyjoke",
    "reactivate": "http://192.168.1.11:9000/www/track/reactivate.js?i=easyjoke",
    "bannerAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_480_80.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg"
    ],
    "floatingAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_480_854.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_540_960.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_720_1280.jpg"
    ]
  },
  {
    "id": 2,
    "type": "app",
    "expires": 1459439999000,
    "reactiveExpires": 1462031999000,
    "inMarket": false,
    "package": "com.vlocker.locker",
    "app": "http://192.168.1.11:9000/www/assets/apk/com.vlocker.locker_2.0.8_208.apk",
    "checksum": "b2ee0034d8c78e736218094f08d44297",
    "cache": "http://192.168.1.11:9000/www/track/cache.js?i=locker",
    "show": "http://192.168.1.11:9000/www/track/show.js?i=locker",
    "click": "http://192.168.1.11:9000/www/track/click.js?i=locker",
    "download": "http://192.168.1.11:9000/www/track/download.js?i=locker",
    "install": "http://192.168.1.11:9000/www/track/install.js?i=locker",
    "activate": "http://192.168.1.11:9000/www/track/activate.js?i=locker",
    "reactivate": "http://192.168.1.11:9000/www/track/reactivate.js?i=locker",
    "bannerAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_480_80.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg"
    ],
    "floatingAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_480_854.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_540_960.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_720_1280.jpg"
    ]
  },
  {
    "id": 3,
    "type": "app",
    "expires": 1459439999000,
    "reactiveExpires": 1462031999000,
    "inMarket": false,
    "package": "com.wiwigo.app",
    "app": "http://192.168.1.11:9000/www/assets/apk/com.wiwigo.app_6.0.6_37.apk",
    "checksum": "2f5c0981a4f22660249db1144659c1b6",
    "cache": "http://192.168.1.11:9000/www/track/cache.js?i=wiwi",
    "show": "http://192.168.1.11:9000/www/track/show.js?i=wiwi",
    "click": "http://192.168.1.11:9000/www/track/click.js?i=wiwi",
    "download": "http://192.168.1.11:9000/www/track/download.js?i=wiwi",
    "install": "http://192.168.1.11:9000/www/track/install.js?i=wiwi",
    "activate": "http://192.168.1.11:9000/www/track/activate.js?i=wiwi",
    "reactivate": "http://192.168.1.11:9000/www/track/reactivate.js?i=wiwi",
    "bannerAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_480_80.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/banner_720_120.jpg"
    ],
    "floatingAd": [
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_480_854.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_540_960.jpg",
      "http://wx-cdn.qtmojo.cn/upload/apk/image/floating_720_1280.jpg"
    ]
  }
]