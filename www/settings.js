{
  "settingsFreq": 3600000,
  "floatingFreq": 3600000,
  "launcherFreq": 3600000,
  "blackList": [
    "cn.cntvnews"
  ],
  "deviceInfo": "http://192.168.1.11:9000/www/device.js",
  "bannerAd": {
    "enable": true,
    "cacheUrl": "",
    "requestUrl": "http://192.168.1.11:9000/www/ad/banner.js",
    "adSpaceId": "spcjMUuZW423a5d3e",
    "freq": 3600000,
    "times": 5
  },
  "floatingAd": {
    "enable": true,
    "cacheUrl": "http://192.168.1.11:9000/www/ad/floating_list.js",
    "requestUrl": "http://192.168.1.11:9000/www/ad/floating.js",
    "adSpaceId": "spcchqS253d56de05",
    "freq": 8388000,
    "duration": 8000,
    "times": 2,
    "maxCachedNum": 5
  },
  "pushAd": {
    "enable": false,
    "adSpaceId": "spcgiKoaU3d56dd34",
    "cacheUrl": "http://192.168.1.11:9000/www/ad/push.js",
    "requestUrl": "",
    "freq": 86400000,
    "times": 3,
    "defaultTitle": "系统推送消息",
    "defaultIcon": ""
  },
  "promotionAd": {
    "enable": true,
    "adTypes": "banner,floating,unlock,home,icon",
    "cacheUrl": "http://192.168.1.11:9000/www/app/cache.js",
    "requestUrl": "http://192.168.1.11:9000/www/app/delivery.js",
    "freq": 86400000,
    "unlockFreq": 10800000,
    "backHomeFreq": 28800000,
    "maxApps": 0
  }
}